///<reference path="../node_modules/grafana-sdk-mocks/app/headers/common.d.ts" />
System.register(['lodash', 'jquery', 'app/core/utils/kbn', 'app/core/config', 'app/core/time_series2', 'app/plugins/sdk'], function(exports_1) {
    var __extends = (this && this.__extends) || function (d, b) {
        for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
    var lodash_1, jquery_1, kbn_1, config_1, time_series2_1, sdk_1;
    var defaultContent, SingleStatCtrl;
    function getColorForValue(data, value) {
        if (!lodash_1.default.isFinite(value)) {
            return null;
        }
        for (var i = data.thresholds.length; i > 0; i--) {
            if (value >= data.thresholds[i - 1]) {
                return data.colorMap[i];
            }
        }
        return lodash_1.default.first(data.colorMap);
    }
    return {
        setters:[
            function (lodash_1_1) {
                lodash_1 = lodash_1_1;
            },
            function (jquery_1_1) {
                jquery_1 = jquery_1_1;
            },
            function (kbn_1_1) {
                kbn_1 = kbn_1_1;
            },
            function (config_1_1) {
                config_1 = config_1_1;
            },
            function (time_series2_1_1) {
                time_series2_1 = time_series2_1_1;
            },
            function (sdk_1_1) {
                sdk_1 = sdk_1_1;
            }],
        execute: function() {
            defaultContent = "\n<style>\n  .wp-title { font-size: 20px; }\n  .wp-value { font-size: 5.25rem; font-weight: 500; line-height: 6.25rem }\n  .wp-value2 { font-size: 3.43rem; line-height: 4.43rem; }\n  .wp-value2-direction { font-size: 1.7rem; }\n  .wp-content { position: relative; z-index: 1; }\n</style>\n\n<div class=\"wp-content\">\n  <div class=\"wp-title\">${title2}</div>\n  <div class=\"wp-value\">${valueFormatted}</div>\n  <div class=\"wp-value2\" style=\"color: ${color2}\"><span class=\"wp-value2-direction\"></span><span class=\"wp-value2-value\">${value2Formatted}</span></div>\n</div>\n\n<script>\n  var charUp = \"&#x25B2;\";\n  var charDown = \"&#x25BC;\";\n  var charRight = \"&#x25B6;\";\n  var diff = ${diff};\n  var value2Elem = this.elem.find(\".wp-value2\");\n\n  var directionChar = charRight;\n\n  var absDiff = Math.abs(diff);\n  if (diff < 0) {\n    directionChar = charDown;\n  } else if( diff > 0) {\n    directionChar = charUp;\n  }\n\n  value2Elem.find(\".wp-value2-direction\").html(directionChar+\"&nbsp;\");\n</script>\n";
            SingleStatCtrl = (function (_super) {
                __extends(SingleStatCtrl, _super);
                /** @ngInject */
                function SingleStatCtrl($scope, $injector, linkSrv, $sanitize) {
                    _super.call(this, $scope, $injector);
                    this.linkSrv = linkSrv;
                    this.$sanitize = $sanitize;
                    this.dataType = 'timeseries';
                    this.valueNameOptions = [
                        { value: 'min', text: 'Min' },
                        { value: 'max', text: 'Max' },
                        { value: 'avg', text: 'Average' },
                        { value: 'current', text: 'Current' },
                        { value: 'total', text: 'Total' },
                        { value: 'name', text: 'Name' },
                        { value: 'first', text: 'First' },
                        { value: 'delta', text: 'Delta' },
                        { value: 'diff', text: 'Difference' },
                        { value: 'range', text: 'Range' },
                        { value: 'last_time', text: 'Time of last point' },
                    ];
                    // Set and populate defaults
                    this.panelDefaults = {
                        content: defaultContent,
                        links: [],
                        datasource: null,
                        maxDataPoints: 100,
                        interval: null,
                        targets: [{}],
                        cacheTimeout: null,
                        format: 'none',
                        prefix: '',
                        postfix: '',
                        nullText: null,
                        valueMaps: [{ value: 'null', op: '=', text: 'N/A' }],
                        mappingTypes: [{ name: 'value to text', value: 1 }, { name: 'range to text', value: 2 }],
                        rangeMaps: [{ from: 'null', to: 'null', text: 'N/A' }],
                        mappingType: 1,
                        nullPointMode: 'connected',
                        valueName: 'avg',
                        prefixFontSize: '50%',
                        valueFontSize: '80%',
                        postfixFontSize: '50%',
                        thresholds: '',
                        colorBackground: false,
                        colorValue: false,
                        colors: ['#299c46', 'rgba(237, 129, 40, 0.89)', '#d44a3a'],
                        sparkline: {
                            show: false,
                            full: false,
                            lineColor: 'rgb(31, 120, 193)',
                            fillColor: 'rgba(31, 118, 189, 0.18)',
                        },
                        gauge: {
                            show: false,
                            minValue: 0,
                            maxValue: 100,
                            thresholdMarkers: true,
                            thresholdLabels: false,
                        },
                        tableColumn: '',
                        title2: '',
                        showDifference: false,
                        absDifference: false,
                    };
                    lodash_1.default.defaults(this.panel, this.panelDefaults);
                    this.events.on('data-received', this.onDataReceived.bind(this));
                    this.events.on('data-error', this.onDataError.bind(this));
                    this.events.on('data-snapshot-load', this.onDataReceived.bind(this));
                    this.events.on('init-edit-mode', this.onInitEditMode.bind(this));
                    this.onSparklineColorChange = this.onSparklineColorChange.bind(this);
                    this.onSparklineFillChange = this.onSparklineFillChange.bind(this);
                }
                SingleStatCtrl.prototype.onInitEditMode = function () {
                    this.fontSizes = ['20%', '30%', '50%', '70%', '80%', '100%', '110%', '120%', '150%', '170%', '200%'];
                    this.addEditorTab('Html Template', 'public/plugins/arakis-dynamichtml-panel/partials/template.html', 2);
                    this.addEditorTab('Options', 'public/plugins/arakis-dynamichtml-panel/partials/editor.html', 3);
                    this.addEditorTab('Value Mappings', 'public/app/plugins/panel/singlestat/mappings.html', 4);
                    this.unitFormats = kbn_1.default.getUnitFormats();
                };
                SingleStatCtrl.prototype.setUnitFormat = function (subItem) {
                    this.panel.format = subItem.value;
                    this.refresh();
                };
                SingleStatCtrl.prototype.onDataError = function (err) {
                    this.onDataReceived([]);
                };
                SingleStatCtrl.prototype.onDataReceived = function (dataList) {
                    this.dataList = dataList;
                    var data = {
                        scopedVars: lodash_1.default.extend({}, this.panel.scopedVars),
                    };
                    if (dataList.length > 0 && dataList[0].type === 'table') {
                        this.dataType = 'table';
                        var tableData = dataList.map(this.tableHandler.bind(this));
                        this.setTableValues(tableData, data);
                    }
                    else {
                        this.dataType = 'timeseries';
                        this.series = dataList.map(this.seriesHandler.bind(this));
                        this.setValues(data);
                    }
                    this.data = data;
                    this.render();
                };
                SingleStatCtrl.prototype.seriesHandler = function (seriesData) {
                    var series = new time_series2_1.default({
                        datapoints: seriesData.datapoints || [],
                        alias: seriesData.target,
                    });
                    series.flotpairs = series.getFlotPairs(this.panel.nullPointMode);
                    return series;
                };
                SingleStatCtrl.prototype.tableHandler = function (tableData) {
                    var datapoints = [];
                    var columnNames = {};
                    tableData.columns.forEach(function (column, columnIndex) {
                        columnNames[columnIndex] = column.text;
                    });
                    this.tableColumnOptions = columnNames;
                    if (!lodash_1.default.find(tableData.columns, ['text', this.panel.tableColumn])) {
                        this.setTableColumnToSensibleDefault(tableData);
                    }
                    tableData.rows.forEach(function (row) {
                        var datapoint = {};
                        row.forEach(function (value, columnIndex) {
                            var key = columnNames[columnIndex];
                            datapoint[key] = value;
                        });
                        datapoints.push(datapoint);
                    });
                    return datapoints;
                };
                SingleStatCtrl.prototype.setTableColumnToSensibleDefault = function (tableData) {
                    if (tableData.columns.length === 1) {
                        this.panel.tableColumn = tableData.columns[0].text;
                    }
                    else {
                        this.panel.tableColumn = lodash_1.default.find(tableData.columns, function (col) {
                            return col.type !== 'time';
                        }).text;
                    }
                };
                SingleStatCtrl.prototype.setTableValues = function (tableData, data) {
                    if (!tableData || tableData.length === 0) {
                        return;
                    }
                    if (tableData[0].length === 0 || tableData[0][0][this.panel.tableColumn] === undefined) {
                        return;
                    }
                    var datapoint = tableData[0][0];
                    data.value = datapoint[this.panel.tableColumn];
                    if (lodash_1.default.isString(data.value)) {
                        data.valueFormatted = lodash_1.default.escape(data.value);
                        data.value = 0;
                        data.valueRounded = 0;
                    }
                    else {
                        var decimalInfo = this.getDecimalsForValue(data.value);
                        var formatFunc = kbn_1.default.valueFormats[this.panel.format];
                        data.valueFormatted = formatFunc(datapoint[this.panel.tableColumn], decimalInfo.decimals, decimalInfo.scaledDecimals);
                        data.valueRounded = kbn_1.default.roundValue(data.value, this.panel.decimals || 0);
                    }
                    this.setValueMapping(data);
                };
                SingleStatCtrl.prototype.canModifyText = function () {
                    return !this.panel.gauge.show;
                };
                SingleStatCtrl.prototype.setColoring = function (options) {
                    if (options.background) {
                        this.panel.colorValue = false;
                        this.panel.colors = ['rgba(71, 212, 59, 0.4)', 'rgba(245, 150, 40, 0.73)', 'rgba(225, 40, 40, 0.59)'];
                    }
                    else {
                        this.panel.colorBackground = false;
                        this.panel.colors = ['rgba(50, 172, 45, 0.97)', 'rgba(237, 129, 40, 0.89)', 'rgba(245, 54, 54, 0.9)'];
                    }
                    this.render();
                };
                SingleStatCtrl.prototype.invertColorOrder = function () {
                    var tmp = this.panel.colors[0];
                    this.panel.colors[0] = this.panel.colors[2];
                    this.panel.colors[2] = tmp;
                    this.render();
                };
                SingleStatCtrl.prototype.onColorChange = function (panelColorIndex) {
                    var _this = this;
                    return function (color) {
                        _this.panel.colors[panelColorIndex] = color;
                        _this.render();
                    };
                };
                SingleStatCtrl.prototype.onSparklineColorChange = function (newColor) {
                    this.panel.sparkline.lineColor = newColor;
                    this.render();
                };
                SingleStatCtrl.prototype.onSparklineFillChange = function (newColor) {
                    this.panel.sparkline.fillColor = newColor;
                    this.render();
                };
                SingleStatCtrl.prototype.getDecimalsForValue = function (value) {
                    if (lodash_1.default.isNumber(this.panel.decimals)) {
                        return { decimals: this.panel.decimals, scaledDecimals: null };
                    }
                    var delta = value / 2;
                    var dec = -Math.floor(Math.log(delta) / Math.LN10);
                    var magn = Math.pow(10, -dec);
                    var norm = delta / magn; // norm is between 1.0 and 10.0
                    var size;
                    if (norm < 1.5) {
                        size = 1;
                    }
                    else if (norm < 3) {
                        size = 2;
                        // special case for 2.5, requires an extra decimal
                        if (norm > 2.25) {
                            size = 2.5;
                            ++dec;
                        }
                    }
                    else if (norm < 7.5) {
                        size = 5;
                    }
                    else {
                        size = 10;
                    }
                    size *= magn;
                    // reduce starting decimals if not needed
                    if (Math.floor(value) === value) {
                        dec = 0;
                    }
                    var result = {};
                    result.decimals = Math.max(0, dec);
                    result.scaledDecimals = result.decimals - Math.floor(Math.log(size) / Math.LN10) + 2;
                    return result;
                };
                SingleStatCtrl.prototype.setValues = function (data) {
                    data.flotpairs = [];
                    /*if (this.series.length > 1) {
                      const error: any = new Error();
                      error.message = 'Multiple Series Error';
                      error.data =
                        'Metric query returns ' +
                        this.series.length +
                        ' series. Single Stat Panel expects a single series.\n\nResponse:\n' +
                        JSON.stringify(this.series);
                      throw error;
                    }*/
                    if (this.series && this.series.length > 0) {
                        var lastPoint = lodash_1.default.last(this.series[0].datapoints);
                        var lastValue = lodash_1.default.isArray(lastPoint) ? lastPoint[0] : null;
                        if (this.panel.valueName === 'name') {
                            data.value = 0;
                            data.valueRounded = 0;
                            data.valueFormatted = this.series[0].alias;
                        }
                        else if (lodash_1.default.isString(lastValue)) {
                            data.value = 0;
                            data.valueFormatted = lodash_1.default.escape(lastValue);
                            data.valueRounded = 0;
                        }
                        else if (this.panel.valueName === 'last_time') {
                            var formatFunc = kbn_1.default.valueFormats[this.panel.format];
                            data.value = lastPoint[1];
                            data.valueRounded = data.value;
                            data.valueFormatted = formatFunc(data.value, this.dashboard.isTimezoneUtc());
                        }
                        else {
                            data.value = this.series[0].stats[this.panel.valueName];
                            data.flotpairs = this.series[0].flotpairs;
                            var decimalInfo = this.getDecimalsForValue(data.value);
                            var formatFunc = kbn_1.default.valueFormats[this.panel.format];
                            data.valueFormatted = formatFunc(data.value, decimalInfo.decimals, decimalInfo.scaledDecimals);
                            data.valueRounded = kbn_1.default.roundValue(data.value, decimalInfo.decimals);
                        }
                        // Add $__name variable for using in prefix or postfix
                        data.scopedVars['__name'] = { value: this.series[0].label };
                    }
                    this.setValueMapping(data);
                };
                SingleStatCtrl.prototype.setValueMapping = function (data) {
                    // check value to text mappings if its enabled
                    if (this.panel.mappingType === 1) {
                        for (var i = 0; i < this.panel.valueMaps.length; i++) {
                            var map = this.panel.valueMaps[i];
                            // special null case
                            if (map.value === 'null') {
                                if (data.value === null || data.value === void 0) {
                                    data.valueFormatted = map.text;
                                    return;
                                }
                                continue;
                            }
                            // value/number to text mapping
                            var value = parseFloat(map.value);
                            if (value === data.valueRounded) {
                                data.valueFormatted = map.text;
                                return;
                            }
                        }
                    }
                    else if (this.panel.mappingType === 2) {
                        for (var i = 0; i < this.panel.rangeMaps.length; i++) {
                            var map = this.panel.rangeMaps[i];
                            // special null case
                            if (map.from === 'null' && map.to === 'null') {
                                if (data.value === null || data.value === void 0) {
                                    data.valueFormatted = map.text;
                                    return;
                                }
                                continue;
                            }
                            // value/number to range mapping
                            var from = parseFloat(map.from);
                            var to = parseFloat(map.to);
                            if (to >= data.valueRounded && from <= data.valueRounded) {
                                data.valueFormatted = map.text;
                                return;
                            }
                        }
                    }
                    if (data.value === null || data.value === void 0) {
                        data.valueFormatted = 'no value';
                    }
                };
                SingleStatCtrl.prototype.removeValueMap = function (map) {
                    var index = lodash_1.default.indexOf(this.panel.valueMaps, map);
                    this.panel.valueMaps.splice(index, 1);
                    this.render();
                };
                SingleStatCtrl.prototype.addValueMap = function () {
                    this.panel.valueMaps.push({ value: '', op: '=', text: '' });
                };
                SingleStatCtrl.prototype.removeRangeMap = function (rangeMap) {
                    var index = lodash_1.default.indexOf(this.panel.rangeMaps, rangeMap);
                    this.panel.rangeMaps.splice(index, 1);
                    this.render();
                };
                SingleStatCtrl.prototype.addRangeMap = function () {
                    this.panel.rangeMaps.push({ from: '', to: '', text: '' });
                };
                SingleStatCtrl.prototype.link = function (scope, elem, attrs, ctrl) {
                    //const $location = this.$location;
                    var linkSrv = this.linkSrv;
                    var $timeout = this.$timeout;
                    var $sanitize = this.$sanitize;
                    var panel = ctrl.panel;
                    var templateSrv = this.templateSrv;
                    var data, linkInfo;
                    var $panelContainer = elem.find('.panel-container');
                    elem = elem.find('.dynamichtml-panel');
                    function applyColoringThresholds(valueString) {
                        var color = getColorForValue(data, data.value);
                        if (color) {
                            return '<span style="color:' + color + '">' + valueString + '</span>';
                        }
                        return valueString;
                    }
                    function getSpan(className, fontSize, applyColoring, value) {
                        value = $sanitize(templateSrv.replace(value, data.scopedVars));
                        value = applyColoring ? applyColoringThresholds(value) : value;
                        return '<span class="' + className + '" style="font-size:' + fontSize + '">' + value + '</span>';
                    }
                    function getBigValueHtml() {
                        var body = '<div class="singlestat-panel-value-container">';
                        if (panel.prefix) {
                            body += getSpan('singlestat-panel-prefix', panel.prefixFontSize, panel.colorPrefix, panel.prefix);
                        }
                        body += getSpan('singlestat-panel-value', panel.valueFontSize, panel.colorValue, data.valueFormatted);
                        if (panel.postfix) {
                            body += getSpan('singlestat-panel-postfix', panel.postfixFontSize, panel.colorPostfix, panel.postfix);
                        }
                        body += '</div>';
                        return body;
                    }
                    function getValueText() {
                        var result = panel.prefix ? templateSrv.replace(panel.prefix, data.scopedVars) : '';
                        result += data.valueFormatted;
                        result += panel.postfix ? templateSrv.replace(panel.postfix, data.scopedVars) : '';
                        return result;
                    }
                    function addGauge() {
                        var width = elem.width();
                        var height = elem.height();
                        // Allow to use a bit more space for wide gauges
                        var dimension = Math.min(width, height * 1.3);
                        ctrl.invalidGaugeRange = false;
                        if (panel.gauge.minValue > panel.gauge.maxValue) {
                            ctrl.invalidGaugeRange = true;
                            return;
                        }
                        var plotCanvas = jquery_1.default('<div></div>');
                        var plotCss = {
                            top: '10px',
                            margin: 'auto',
                            position: 'relative',
                            height: height * 0.9 + 'px',
                            width: dimension + 'px',
                        };
                        plotCanvas.css(plotCss);
                        var thresholds = [];
                        for (var i = 0; i < data.thresholds.length; i++) {
                            thresholds.push({
                                value: data.thresholds[i],
                                color: data.colorMap[i],
                            });
                        }
                        thresholds.push({
                            value: panel.gauge.maxValue,
                            color: data.colorMap[data.colorMap.length - 1],
                        });
                        var bgColor = config_1.default.bootData.user.lightTheme ? 'rgb(230,230,230)' : 'rgb(38,38,38)';
                        var fontScale = parseInt(panel.valueFontSize, 10) / 100;
                        var fontSize = Math.min(dimension / 5, 100) * fontScale;
                        // Reduce gauge width if threshold labels enabled
                        var gaugeWidthReduceRatio = panel.gauge.thresholdLabels ? 1.5 : 1;
                        var gaugeWidth = Math.min(dimension / 6, 60) / gaugeWidthReduceRatio;
                        var thresholdMarkersWidth = gaugeWidth / 5;
                        var thresholdLabelFontSize = fontSize / 2.5;
                        var options = {
                            series: {
                                gauges: {
                                    gauge: {
                                        min: panel.gauge.minValue,
                                        max: panel.gauge.maxValue,
                                        background: { color: bgColor },
                                        border: { color: null },
                                        shadow: { show: false },
                                        width: gaugeWidth,
                                    },
                                    frame: { show: false },
                                    label: { show: false },
                                    layout: { margin: 0, thresholdWidth: 0 },
                                    cell: { border: { width: 0 } },
                                    threshold: {
                                        values: thresholds,
                                        label: {
                                            show: panel.gauge.thresholdLabels,
                                            margin: thresholdMarkersWidth + 1,
                                            font: { size: thresholdLabelFontSize },
                                        },
                                        show: panel.gauge.thresholdMarkers,
                                        width: thresholdMarkersWidth,
                                    },
                                    value: {
                                        color: panel.colorValue ? getColorForValue(data, data.valueRounded) : null,
                                        formatter: function () {
                                            return getValueText();
                                        },
                                        font: {
                                            size: fontSize,
                                            family: '"Helvetica Neue", Helvetica, Arial, sans-serif',
                                        },
                                    },
                                    show: true,
                                },
                            },
                        };
                        elem.append(plotCanvas);
                        var plotSeries = {
                            data: [[0, data.value]],
                        };
                        jquery_1.default.plot(plotCanvas, [plotSeries], options);
                    }
                    function addSparkline() {
                        var width = elem.width() + 20;
                        if (width < 30) {
                            // element has not gotten it's width yet
                            // delay sparkline render
                            setTimeout(addSparkline, 30);
                            return;
                        }
                        var height = ctrl.height;
                        var plotCanvas = jquery_1.default('<div></div>');
                        var plotCss = {};
                        plotCss.position = 'absolute';
                        if (panel.sparkline.full) {
                            plotCss.bottom = '5px';
                            plotCss.left = '-5px';
                            plotCss.width = width - 10 + 'px';
                            var dynamicHeightMargin = height <= 100 ? 5 : Math.round(height / 100) * 15 + 5;
                            plotCss.height = height - dynamicHeightMargin + 'px';
                        }
                        else {
                            plotCss.bottom = '0px';
                            plotCss.left = '-5px';
                            plotCss.width = width - 10 + 'px';
                            plotCss.height = Math.floor(height * 0.25) + 'px';
                        }
                        plotCanvas.css(plotCss);
                        var options = {
                            legend: { show: false },
                            series: {
                                lines: {
                                    show: true,
                                    fill: 1,
                                    zero: false,
                                    lineWidth: 1,
                                    fillColor: panel.sparkline.fillColor,
                                },
                            },
                            yaxes: { show: false },
                            xaxis: {
                                show: false,
                                mode: 'time',
                                min: ctrl.range.from.valueOf(),
                                max: ctrl.range.to.valueOf(),
                            },
                            grid: { hoverable: false, show: false },
                        };
                        elem.append(plotCanvas);
                        var plotSeries = {
                            data: data.flotpairs,
                            color: panel.sparkline.lineColor,
                        };
                        jquery_1.default.plot(plotCanvas, [plotSeries], options);
                    }
                    function render() {
                        if (!ctrl.data) {
                            return;
                        }
                        data = ctrl.data;
                        // get thresholds
                        data.thresholds = panel.thresholds.split(',').map(function (strVale) {
                            return Number(strVale.trim());
                        });
                        data.colorMap = panel.colors;
                        var body = panel.gauge.show ? '' : getBigValueHtml();
                        if (panel.colorBackground) {
                            var color = getColorForValue(data, data.value);
                            if (color) {
                                $panelContainer.css('background-color', color);
                                if (scope.fullscreen) {
                                    elem.css('background-color', color);
                                }
                                else {
                                    elem.css('background-color', '');
                                }
                            }
                        }
                        else {
                            $panelContainer.css('background-color', '');
                            elem.css('background-color', '');
                        }
                        var dataList = ctrl.dataList;
                        var content = panel.content;
                        content = templateSrv.replace(content, data.scopedVars);
                        var re = null;
                        var match = null;
                        var prev = null;
                        var last = null;
                        var current = data.valueRounded; // alias "value1"
                        var value2 = null;
                        if (dataList != null && dataList.length > 0) {
                            last = getValue(dataList, 'value', 0, 'last');
                            prev = getValue(dataList, 'prev', 0, 'last');
                            if (prev == null)
                                prev = getValue(dataList, 'value', 0, 'prev');
                            value2 = ctrl.panel.showDifference ? current - prev : prev;
                            re = /\$\{([ \.=\w]+)\}/g;
                            match = null;
                            while (match = re.exec(content)) {
                                var value = null;
                                var varName = match[1];
                                var defaultValue = 0;
                                if (varName == 'title2')
                                    defaultValue = ctrl.panel.title2;
                                value = getValue(dataList, varName, defaultValue, 'last');
                                if (varName == 'prev')
                                    value = prev;
                                if (varName == 'last')
                                    value = last;
                                if (varName == 'diff')
                                    value = current - prev;
                                if (varName == 'value2')
                                    value = value2;
                                if (varName == 'value')
                                    value = current;
                                if (varName == 'value2Formatted')
                                    value = ctrl.panel.absDifference ? Math.abs(value2) : value2;
                                if (varName == 'color2')
                                    value = getColorForValue(data, value2);
                                if (varName == 'valueFormatted' && data.valueFormatted != null)
                                    value = data.valueFormatted;
                                if (value != null) {
                                    content = content.substr(0, match.index) + value + content.substr(match.index + match[0].length);
                                }
                            }
                        }
                        re = /\$\{([ \.=\w]+)\}/g;
                        while (match = re.exec(content)) {
                            var value = null;
                            varName = match[1];
                            if (varName == 'title2')
                                value = ctrl.panel.title2;
                            if (value == null)
                                value = 0;
                            content = content.substr(0, match.index) + value + content.substr(match.index + match[0].length);
                        }
                        var scripts = [];
                        ctrl.elem = elem;
                        re = /\<script\>((.|\n)*)\<\/script\>/g;
                        match = null;
                        while (match = re.exec(content)) {
                            // remove script tag
                            content = content.substr(0, match.index) + content.substr(match.index + match[0].length);
                            var script = match[1];
                            scripts.push(script);
                        }
                        elem.html(content);
                        console.log(content);
                        for (var i = 0; i < scripts.length; i++) {
                            try {
                                var func = new Function(scripts[i]);
                                func.call(ctrl);
                            }
                            catch (e) {
                                console.log(scripts[i]);
                                console.log(e);
                                throw e;
                            }
                        }
                        if (panel.sparkline.show) {
                            addSparkline();
                        }
                        if (panel.gauge.show) {
                            addGauge();
                        }
                        elem.toggleClass('pointer', panel.links.length > 0);
                        if (panel.links.length > 0) {
                            linkInfo = linkSrv.getPanelLinkAnchorInfo(panel.links[0], data.scopedVars);
                        }
                        else {
                            linkInfo = null;
                        }
                    }
                    function getValue(dataList, varName, defaultValue, take) {
                        for (var i = dataList.length - 1; i >= 0; i--) {
                            var dl = dataList[i];
                            var rowIndex = null;
                            if (dl.rows != null) {
                                if (take == 'last')
                                    rowIndex = dl.rows.length - 1;
                                if (take == 'prev')
                                    rowIndex = dl.rows.length - 2;
                            }
                            else {
                                if (take == 'last')
                                    rowIndex = dl.datapoints.length - 1;
                                if (take == 'prev')
                                    rowIndex = dl.datapoints.length - 2;
                            }
                            var value = getValueFromRecord(dl, varName, defaultValue, rowIndex);
                            if (value != null)
                                return value;
                        }
                        return null;
                    }
                    function getValueFromRecord(dl, varName, defaultValue, rowIndex) {
                        var value = null;
                        if (dl.rows != null) {
                            if (rowIndex == null)
                                rowIndex = dl.rows.length - 1;
                            var colNum = dl.columns.findIndex(function (a) { return a.text == varName; });
                            if (colNum >= 0) {
                                if (rowIndex < 0 || rowIndex > dl.rows.length - 1)
                                    return defaultValue;
                                return dl.rows[rowIndex][colNum];
                            }
                            return null;
                        }
                        else if (dl.datapoints != null) {
                            if (rowIndex == null)
                                rowIndex = dl.datapoints.length - 1;
                            if (dl.target == varName) {
                                if (rowIndex < 0 || rowIndex > dl.datapoints.length - 1)
                                    return defaultValue;
                                return dl.datapoints[rowIndex][0];
                            }
                            else {
                                return null;
                            }
                        }
                    }
                    function hasValue(dataList, varName) {
                        return getValue(dataList, varName, null, 'auto') != null;
                    }
                    function hookupDrilldownLinkTooltip() {
                        // drilldown link tooltip
                        var drilldownTooltip = jquery_1.default('<div id="tooltip" class="">hello</div>"');
                        elem.mouseleave(function () {
                            if (panel.links.length === 0) {
                                return;
                            }
                            $timeout(function () {
                                drilldownTooltip.detach();
                            });
                        });
                        elem.click(function (evt) {
                            if (!linkInfo) {
                                return;
                            }
                            // ignore title clicks in title
                            if (jquery_1.default(evt).parents('.panel-header').length > 0) {
                                return;
                            }
                            if (linkInfo.target === '_blank') {
                                window.open(linkInfo.href, '_blank');
                                return;
                            }
                            if (linkInfo.href.indexOf('http') === 0) {
                                window.location.href = linkInfo.href;
                            }
                            else {
                                $timeout(function () {
                                    //$location.url(linkInfo.href);
                                });
                            }
                            drilldownTooltip.detach();
                        });
                        elem.mousemove(function (e) {
                            if (!linkInfo) {
                                return;
                            }
                            drilldownTooltip.text('click to go to: ' + linkInfo.title);
                            drilldownTooltip.place_tt(e.pageX, e.pageY - 50);
                        });
                    }
                    hookupDrilldownLinkTooltip();
                    this.events.on('render', function () {
                        render();
                        ctrl.renderingCompleted();
                    });
                };
                SingleStatCtrl.templateUrl = 'partials/module.html';
                return SingleStatCtrl;
            })(sdk_1.MetricsPanelCtrl);
            exports_1("SingleStatCtrl", SingleStatCtrl);
            exports_1("PanelCtrl", SingleStatCtrl);
            exports_1("getColorForValue", getColorForValue);
        }
    }
});
//# sourceMappingURL=module.js.map