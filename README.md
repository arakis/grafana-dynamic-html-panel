Dynamic Html Panel has the following Features:

* All Features like Singlestat
* Defining a calculated secondary value, for example the previous value in the timeline
* Show difference from current to previous value.
* By editing the Html template, you can customize the look and feel
* Using Placeholders in the Html template, you can access all columns of the datasource.

To use Placeholders within the template, you have to write them in this syntax: `${placeholdername}`, for example `${value}`

| Name | Description  |
| ------------- |-------------|
| value | primary value, like Singlestat |
| value2 | secondary value |
| value2Formatted | Formatted secondary value |
| last | last value in a timeline |
| prev | previous value in a timeline |
| color2 | threshold color of secondary value |
| valueFormatted | formatted primary value |
| title2 | A bigger, additional title. Usefull, when panel title is too small for you |

#### Editor options
* **Title 2:** If set, an additional title will be shown
* **Show Difference:** By default, value2 is the previous value. If checked, value 2 is the difference between prevous and current value.
* **Absolute Difference:** If checked, no negative numbers will be shown.

Sample Query:
```sql
SELECT
  $__timeGroup(timestamp, $interval) AS time,
  SUM(value) AS value
FROM
  metrics
WHERE
  $__timeFilter(timestamp)
GROUP BY $__timeGroup(timestamp, $interval)
ORDER BY time
```
value and time are mandantory.

By Default, most of the values are looked up and calculated automaticly. But if the placeholders name equals to a column in the query result, that value will be used.

Sample template:

```html
<style>
  .wp-title { font-size: 20px; }
  .wp-value { font-size: 5.25rem; font-weight: 500; line-height: 6.25rem }
  .wp-value2 { font-size: 3.43rem; line-height: 4.43rem; }
  .wp-value2-direction { font-size: 1.7rem; }
  .wp-content { position: relative; z-index: 1; }
</style>

<div class="wp-content">
  <div class="wp-title">${title2}</div>
  <div class="wp-value">${valueFormatted}</div>
  <div class="wp-value2" style="color: ${color2}"><span class="wp-value2-direction"></span><span class="wp-value2-value">${value2Formatted}</span></div>
</div>

<script>
  var charUp = "&#x25B2;";
  var charDown = "&#x25BC;";
  var charRight = "&#x25B6;";
  var diff = ${diff};
  var value2Elem = this.elem.find(".wp-value2");

  var directionChar = charRight;

  var absDiff = Math.abs(diff);
  if (diff < 0) {
    directionChar = charDown;
  } else if( diff > 0) {
    directionChar = charUp;
  }

  value2Elem.find(".wp-value2-direction").html(directionChar+"&nbsp;");
</script>
```

Instead of copying the template to every panel, you can use a variable named `$template`. Simple use only `$template` in the Editor in the Html Template section. Do not forget to define the variable in the dashboard, and choose `Constant` type and write the template in one line.